<?php

namespace Drupal\backlinks_index\Batch;

use Drupal\Core\Site\Settings;

/**
 * Runs a single scan batch.
 */
class BacklinksScanBatch {

  /**
   * Runs a single scan batch process.
   *
   * @param array $context
   *   The batch context.
   */
  public static function run(array &$context) {
    $sandbox = &$context['sandbox'];

    /** @var Drupal\Core\Entity\Query\QueryInterface $entityQuery */
    $entityQuery = \Drupal::entityQuery('node');
    /** @var \Drupal\backlinks\Services\BacklinksManager $backlinksManager */
    $backlinksManager = \Drupal::service('backlinks_index.manager');

    $allowedBundles = $backlinksManager->getAllowedBundles();

    if (!isset($sandbox['max'])) {
      $ids = $entityQuery->accessCheck(FALSE)->execute();
      $backlinksManager->truncateBacklinks();

      if (empty($ids) || empty($allowedBundles)) {
        $context['finished'] = 1;
        $context['message'] = t('Nothing to scan.');

        return;
      }

      $sandbox['progress'] = 0;
      $sandbox['max'] = count($ids);
    }

    $batch_size = Settings::get('entity_update_batch_size', 20);
    $idsToProcess = $entityQuery->range($sandbox['progress'], $batch_size)
      ->accessCheck(FALSE)
      ->execute();

    if (empty($idsToProcess)) {
      $context['finished'] = 1;
      $context['message'] = t('Nothing to scan.');

      return;
    }

    /** @var \Drupal\Core\Entity\EntityTypeManager $entityTypeManager */
    $entityTypeManager = \Drupal::entityTypeManager();
    /** @var \Drupal\Core\Language\LanguageManager $languageManager */
    $languageManager = \Drupal::service('language_manager');

    foreach ($idsToProcess as $id) {
      /** @var \Drupal\node\NodeInterface $node */
      if ($node = $entityTypeManager->getStorage('node')->load($id)) {
        if (in_array($node->bundle(), $allowedBundles)) {
          foreach (array_keys($languageManager->getLanguages()) as $lang) {
            if ($node->hasTranslation($lang)) {
              $backlinksManager->scan($node->getTranslation($lang));
            }
          }
        }
      }

      $sandbox['progress']++;
    }

    $context['finished'] = $sandbox['progress'] < $sandbox['max'] ? ($sandbox['progress'] / $sandbox['max']) : 1;
  }

  /**
   * Callback executed when BacklinksScan batch process completes.
   *
   * @param bool $success
   *   TRUE if batch successfully completed.
   * @param array $results
   *   Batch results.
   * @param array $operations
   *   An array of methods triggered in the batch.
   * @param string $elapsed
   *   The time to run the batch.
   */
  public static function finished($success, array $results, array $operations, $elapsed) {
    if ($success) {
      /** @var \Drupal\backlinks\Services\BacklinksManager $backlinksManager */
      $backlinksManager = \Drupal::service('backlinks_index.manager');
      /** @var \Drupal\Core\Messenger\Messenger $messenger */
      $messenger = \Drupal::messenger();

      $messenger->addStatus(\Drupal::translation()
        ->formatPlural($backlinksManager->getIndexSize('nodes'), 'Scanned 1 node.', 'Scanned @count nodes.'));

      drupal_flush_all_caches();
    }
  }

}
