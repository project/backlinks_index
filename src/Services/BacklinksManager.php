<?php

namespace Drupal\backlinks_index\Services;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ThemeInitialization;
use Drupal\Core\Theme\ThemeManager;
use Drupal\node\NodeInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\redirect\RedirectRepository;

/**
 * Class BacklinksManager is a helper for scanning and building backlinks list.
 */
class BacklinksManager {

  use StringTranslationTrait;
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  private $languageManager;

  /**
   * The Path Alias Manager Interface.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $pathAliasManager;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManager
   */
  private $themeManager;

  /**
   * The theme initialization.
   *
   * @var \Drupal\Core\Theme\ThemeInitialization
   */
  private $themeInitialization;

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $connection;

  /**
   * The redirect repository.
   *
   * @var \Drupal\redirect\RedirectRepository
   */
  protected $redirectRepository;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  private $config;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Constructor for the manager.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   * @param \Drupal\path_alias\AliasManagerInterface $path_alias_manager
   *   The path alias manager.
   * @param \Drupal\Core\Theme\ThemeManager $theme_manager
   *   The theme manager.
   * @param \Drupal\Core\Theme\ThemeInitialization $theme_initialization
   *   The theme initialization.
   * @param \Drupal\redirect\RedirectRepository $redirect_repository
   *   The redirect repository.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(Connection $database, EntityTypeManager $entity_manager, LanguageManager $language_manager, AliasManagerInterface $path_alias_manager, ThemeManager $theme_manager, ThemeInitialization $theme_initialization, RedirectRepository $redirect_repository, RendererInterface $renderer, ConfigFactory $config_factory, LoggerChannelFactoryInterface $logger_factory) {
    $this->connection = $database;
    $this->entityTypeManager = $entity_manager;
    $this->languageManager = $language_manager;
    $this->pathAliasManager = $path_alias_manager;
    $this->themeManager = $theme_manager;
    $this->themeInitialization = $theme_initialization;
    $this->redirectRepository = $redirect_repository;
    $this->renderer = $renderer;
    $this->config = $config_factory;
    $this->logger = $logger_factory->get('backlinks');
  }

  /**
   * Scan rendered node content and parse internal links.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   */
  public function scan(NodeInterface $node) {
    $matches = [];
    $newLines = ["\r", "\n"];

    $activeTheme = $this->themeManager->getActiveTheme();
    $defaultTheme = $this->themeInitialization
      ->getActiveThemeByName($this->config->get('system.theme')
        ->get('default'));

    try {
      // Switch to default for rendering node content in frontend styles.
      $this->themeManager->setActiveTheme($defaultTheme);
      drupal_static_reset('template_preprocess');

      // Render node.
      $build = $this->entityTypeManager->getViewBuilder('node')->view($node);
      $render = $this->renderer->renderPlain($build);

      // Switch theme back.
      $this->themeManager->setActiveTheme($activeTheme);
      drupal_static_reset('template_preprocess');
    }
    catch (\Exception $exception) {
      $this->logger->notice('Issue crawling node @nid - @langcode', [
        '@nid' => $node->id(),
        '@langcode' => strtoupper($node->language()->getId()),
      ]);

      return;
    }

    // Clean up current backlinks.
    $this->deleteBacklink($node);

    // Remove comments and newlines.
    $html = str_replace($newLines, '', preg_replace('~<!--(.*?)-->~s', '', $render->__toString()));

    // Search for href links.
    preg_match_all('/href=".*?"/', $html, $matches);

    if (empty($matches)) {
      return;
    }

    foreach ($matches as $match) {
      foreach ($match as $link) {
        $url = ltrim(preg_replace('/#.*/', '', (str_replace('"', '', str_replace('href=', '', $link)))), '/');

        // Skip absolute urls.
        if (filter_var($url, FILTER_VALIDATE_URL)) {
          continue;
        }

        $path = explode('/', $url);

        // Remove language prefix from url.
        if (in_array($path[0], array_keys($this->languageManager->getLanguages()))) {
          array_shift($path);
        }

        // Path is node.
        if ($path[0] == 'node' && is_numeric($path[1]) && count($path) == 2) {
          $id = $path[1];
        }
        else {
          // Lookup for alias.
          $alias = '/' . implode('/', $path);
          $id = $this->aliasLookup($node, $alias);
        }

        // Exclude self.
        if ($id && $id != $node->id()) {
          $this->createBacklink($node, $id);
        }
      }
    }
  }

  /**
   * Function to return allowed bundles from config.
   */
  public function getAllowedBundles() {
    $bundles = $this->config->getEditable('backlinks_index.settings')
      ->get('bundles');

    return isset($bundles['selected']) ? array_filter($bundles['selected'], function ($value) {
      return ($value !== 0);
    }) : [];
  }

  /**
   * Function to truncate backlinks.
   *
   * @param string|null $dimension
   *   The counts key to return.
   */
  public function getIndexSize($dimension = NULL) {
    $counts = [
      'backlinks' => count($this->getBacklinks()),
      'nodes' => count($this->getBacklinks(NULL, TRUE)),
    ];

    if (empty($dimension)) {
      return $counts;
    }

    return isset($counts[$dimension]) ?: 0;
  }

  /**
   * Build backlink list table.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   *
   * @return array
   *   List of backlinks.
   */
  public function getList(NodeInterface $node) {
    $rows = [];

    $results = $this->getBacklinks($node);
    $langcode = $node->language()->getId();

    $header = [
      'title' => $this->t('Title'),
      'occurrence' => $this->t('Occurrence'),
      'type' => $this->t('Type'),
      'status' => $this->t('Status'),
    ];

    if (!empty($results)) {
      foreach ($results as $result) {
        /** @var \Drupal\node\NodeInterface $entity */
        if ($entity = $this->entityTypeManager->getStorage('node')
          ->load($result->backlink)) {
          // Check for translation.
          if ($entity->hasTranslation($langcode)) {
            $entity = $entity->getTranslation($langcode);
          }

          // Calculate occurrence.
          $occurrence[$entity->id()] = isset($occurrence[$entity->id()]) ? $occurrence[$entity->id()] + 1 : 1;

          // Build row data.
          $rows[$entity->id()] = [
            'title' => Link::fromTextAndUrl($entity->label(), $entity->toUrl('edit-form', ['attributes' => ['target' => '_blank']])),
            'occurrence' => $occurrence[$entity->id()] . 'x',
            'type' => $entity->type->entity->label(),
            'status' => $entity->isPublished() ? $this->t('Published') : $this->t('Unpublished'),
          ];
        }
      }
    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => array_values($rows),
      '#empty' => $this->t('No backlinks has been found.'),
    ];

    return $build;
  }

  /**
   * Function to truncate backlinks.
   */
  public function truncateBacklinks() {
    $this->connection->truncate('backlinks')->execute();
    $this->connection->update('node_field_data')
      ->fields(['backlinks' => 0])
      ->execute();

    drupal_flush_all_caches();
  }

  /**
   * Lookup for node id by path alias.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   * @param string $alias
   *   The path alias.
   *
   * @return int|false
   *   Node ID.
   */
  private function aliasLookup(NodeInterface $node, $alias) {
    $path = $this->pathAliasManager->getPathByAlias($alias);

    // Check path alias.
    if ($id = $this->lookupAliasMatch($node, $path)) {
      return $id;
    }

    // If no alias found, try in redirects.
    if ($source = $this->redirectRepository->findBySourcePath(ltrim($alias, '/'))) {
      $redirect = reset($source);
      $path = str_replace('internal:', '', $redirect->getRedirect()['uri']);

      if ($id = $this->lookupAliasMatch($node, $path)) {
        return $id;
      }
    }

    return FALSE;
  }

  /**
   * Helper function to do preg_match on path.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   * @param string $path
   *   The path.
   *
   * @return int|false
   *   Node ID.
   */
  private function lookupAliasMatch(NodeInterface $node, $path) {
    // Lookup only for node paths.
    if (preg_match('/node\/(\d+)/', $path, $matches)) {
      // Exclude self.
      if ($node->id() != $matches[1]) {
        return $matches[1];
      }
    }

    return FALSE;
  }

  /**
   * Function to get backlinks.
   *
   * @param \Drupal\node\NodeInterface|null $node
   *   The node Object.
   * @param bool $grouping
   *   The query grouping control.
   *
   * @return array
   *   The list of backlinks for requested node.
   */
  public function getBacklinks(NodeInterface $node = NULL, $grouping = FALSE) {
    $fields = $grouping ? ['source'] : [];
    $query = $this->connection->select('backlinks', 'b')->fields('b', $fields);

    if (!empty($node)) {
      $query->condition('source', $node->id());
      $query->condition('langcode', $node->language()->getId());
    }

    if ($grouping) {
      $query->groupBy('b.source');
    }

    return $query->execute()->fetchAll();
  }

  /**
   * Function to create backlink.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   * @param int $source
   *   The source id.
   */
  private function createBacklink(NodeInterface $node, $source) {
    $langcode = $node->language()->getId();

    $this->connection->insert('backlinks')->fields([
      'source' => $source,
      'backlink' => $node->id(),
      'langcode' => $langcode,
      'timestamp' => time(),
    ])->execute();

    // Update node table by source.
    $this->updateNodeField([$source], $langcode, 1);
  }

  /**
   * Function to delete backlink.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   */
  private function deleteBacklink(NodeInterface $node) {
    // Collect sources to update later by backlinks.
    $nids = $this->deleteBacklinkCollectSources($node);
    $langcode = $node->language()->getId();

    // Delete  backlinks.
    $this->connection->delete('backlinks')
      ->condition('backlink', $node->id())
      ->condition('langcode', $langcode)
      ->execute();

    // Update node table by previously collected sources.
    if (!empty($nids)) {
      $this->updateNodeField($nids, $langcode, 0);
    }
  }

  /**
   * Function to collect backlink sources to delete.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   *
   * @return array
   *   The removed backlinks ids.
   */
  private function deleteBacklinkCollectSources(NodeInterface $node) {
    $ids = [];

    $results = $this->connection->select('backlinks', 'b')
      ->fields('b')
      ->condition('backlink', $node->id())
      ->condition('langcode', $node->language()->getId())
      ->execute()
      ->fetchAll();

    if (!empty($results)) {
      foreach ($results as $result) {
        $ids[] = $result->source;
      }
    }

    return array_unique($ids);
  }

  /**
   * Function to update  backlink value in node_field_data.
   *
   * @param array $ids
   *   The node ids.
   * @param string $langcode
   *   The langcode.
   * @param int $value
   *   The backlink id.
   */
  private function updateNodeField(array $ids, $langcode, $value) {
    $this->connection->update('node_field_data')
      ->fields(['backlinks' => $value])
      ->condition('nid', $ids, 'IN')
      ->condition('langcode', $langcode)
      ->execute();

    $this->entityTypeManager->getStorage('node')->resetCache($ids);
  }

}
