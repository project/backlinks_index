<?php

namespace Drupal\backlinks_index\Controller;

use Drupal\backlinks_index\Services\BacklinksManager;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller to show node Backlinks list.
 */
class BacklinksController extends ControllerBase {

  /**
   * The backlinks manager.
   *
   * @var \Drupal\backlinks_index\Services\BacklinksManager
   */
  protected $backlinksManager;

  /**
   * Constructs a BacklinksController object.
   *
   * @param \Drupal\backlinks_index\Services\BacklinksManager $backlinks_manager
   *   The backlinks manager.
   */
  public function __construct(BacklinksManager $backlinks_manager) {
    $this->backlinksManager = $backlinks_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('backlinks_index.manager'),
    );
  }

  /**
   * Return table view of node backlinks.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HttpRequest object representing the current request.
   *
   * @return array
   *   Table with results
   */
  public function content(NodeInterface $node, Request $request) {
    return $this->backlinksManager->getList($node);
  }

}
