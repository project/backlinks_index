<?php

namespace Drupal\backlinks_index\Form;

use Drupal\backlinks_index\Batch\BacklinksScanBatch;
use Drupal\backlinks_index\Services\BacklinksManager;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Backlinks module routes.
 */
class BacklinksSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The backlinks manager.
   *
   * @var \Drupal\backlinks_index\Services\BacklinksManager
   */
  protected $backlinksManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new \Drupal\backlinks_index\Form\BacklinksSettingsForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\backlinks_index\Services\BacklinksManager $backlinks_manager
   *   The backlinks manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, BacklinksManager $backlinks_manager, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->backlinksManager = $backlinks_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('backlinks_index.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['backlinks_index.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'backlinks_index_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $indexSize = $this->backlinksManager->getIndexSize();

    $form['status'] = [
      '#type' => 'container',
      '#markup' => $this->t('@backlinks backlinks indexed for @nodes nodes.', [
        '@backlinks' => $indexSize['backlinks'],
        '@nodes' => $indexSize['nodes'],
      ]),
      '#attributes' => [
        'class' => [
          'messages',
          $indexSize['backlinks'] > 0 ? 'messages--status' : 'messages--error',
        ],
      ],
    ];

    $form['bundles'] = [
      '#type' => 'details',
      '#title' => $this->t('Bundles'),
      '#open' => TRUE,
    ];

    $form['bundles']['selected'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select bundles to scan for backlinks'),
      '#options' => $this->getBundles(),
      '#default_value' => array_keys($this->backlinksManager->getAllowedBundles()),
      '#multiple' => TRUE,
    ];

    $form['actions']['#type'] = 'container';

    $form['actions']['purge'] = [
      '#type' => 'submit',
      '#value' => $this->t('Purge'),
      '#name' => 'purge',
    ];

    $form['actions']['reindex'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reindex'),
      '#name' => 'reindex',
    ];

    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#name' => 'save',
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('backlinks_index.settings');
    $op = $form_state->getTriggeringElement();

    switch ($op['#name']) {
      case 'save':
        $config->set('bundles', ['selected' => $form_state->getValue('selected')]);
        $config->save();
        break;

      case 'reindex':
        /** @var \Drupal\Core\Batch\BatchBuilder $builder */
        $builder = (new BatchBuilder());
        $builder->setTitle($this->t('Scanning nodes'))
          ->addOperation([BacklinksScanBatch::class, 'run'])
          ->setFinishCallback([BacklinksScanBatch::class, 'finished']);

        batch_set($builder->toArray());
        break;

      case 'purge':
        $this->backlinksManager->truncateBacklinks();
        $this->messenger->addStatus($this->t('Backlink index purged.'));
        break;
    }

  }

  /**
   * Helper function to get node bundle options.
   */
  private function getBundles() {
    $options = [];
    $types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    foreach ($types as $node_type) {
      $options[$node_type->id()] = $node_type->label();
    }

    return $options;
  }

}
