# Backlinks Index

The Backlinks Index module displays the list of link to current node
from allowed node bundles.

- For a full description of the module, visit the [project page](https://www.drupal.org/project/backlinks_index).
- Use the [Issue queue](https://www.drupal.org/project/issues/backlinks_index) to submit bug reports and feature suggestions, or track changes.


## Contents of this file

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

* Latest dev release of Drupal 8.x.
* [Hook Post Action](https://www.drupal.org/project/hook_post_action)
* [Redirect](https://www.drupal.org/project/redirect)


## Installation

* Install as you would normally install a contributed Drupal module. See [installing modules](https://www.drupal.org/node/895232) for further information.


## Configuration

Users in roles with the 'Administer Backlinks' permission will be able to
manage the module settings. Users with 'Access Backlinks' permission
will see the list of backlinks on node tab. Configure permissions as
usual at:

* Administration » People » Permissions
* admin/people/permissions

From the module settings page, customize allowed node bundles for
backlinks scan. See:

* Administration » Configuration » Content authoring » Backlinks
* /admin/config/content/backlinks


## Maintainers

Current maintainers:
* Matej Lehotsky - [@matej.lehotsky](https://www.drupal.org/u/matejlehotsky)
